$(document).ready(function(){

  var isPasswordShow = false;
  $('#togglePass').click(function(){

    if (!isPasswordShow) {
      var temp = $('#real-input').val();
      $('#instead-input').val(temp);
      $('#real-input').css({'display':'none'});
      $('#instead-input').css({'display':'block'});
      $('#togglePass svg').css({'color':'lightgreen'});
      isPasswordShow = !isPasswordShow;
      return;
    }

    if (isPasswordShow) {
      var temp = $('#instead-input').val();
      $('#real-input').val(temp);
      $('#real-input').css({'display':'block'});
      $('#instead-input').css({'display':'none'});
      $('#togglePass svg').css({'color':'red'});
      isPasswordShow = !isPasswordShow;
      return;
    }

  });

  /* ↓↓↓ slick carousell ↓↓↓ */
  $('.slick_money__slick').slick({
    arrows: false,
    dots: false,
    autoplay: true,
    autoplaySpeed: 3000,
    pauseOnFocus: false,
    slidesToShow: 6,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 5
        }
      },
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 4
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 1
        }
      },
    ]
  });
  /* ↑↑↑ /slick carousell ↑↑↑ */

});